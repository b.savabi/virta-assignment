
# Virta assignment - UI

## Client 
__UI: (Deployed on Google Firebase)__

[https://virta-assignment-d22a2.web.app/](https://virta-assignment-d22a2.web.app/)

__UI source code__

[https://gitlab.com/b.savabi/virta-assignment](https://gitlab.com/b.savabi/virta-assignment)

## GraphQL server

__GraphQL server endpoint: (Running on Heroku)__

[https://virta-assignment.herokuapp.com/](https://virta-assignment.herokuapp.com/)

__GraphQL server source code__

[https://gitlab.com/b.savabi/virta-assignment-graphql-server](https://gitlab.com/b.savabi/virta-assignment-graphql-server)



## Toolchain (modified react-create-app)

- TypeScript
- React v17.0.2
- GraphQL
- Apollo client/server
- styled-components
- eslint, prettier
- testing-library (Jest)

## Key Features

- Requested features are delivered
- Accessibility consideration (tested by https://tenon.io/ - W3C recommendation)
- Components and views have ~100% test coverage 
- Simple keyboard navigation is implemented 
- Tokenized style implementation using styled-components  
- Icon SVGs are made using Adobe Illustrator


## Available Scripts

In the project directory, you can run:

### `npm run start`
Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm run test`
Launches the test runner in the interactive watch mode.

### `npm run lint`
To run lint tests.

### `npm run build`
Builds the app for production to the `build` folder.

## Changelog

### v0.2.0
- Updated the UI to match with latest GraphQL server handling REST API.

### v0.1.0
- Initial version working with mock data received from a GraphQL endpoint.