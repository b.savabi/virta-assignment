import React from 'react';
import { render, waitFor, fireEvent } from '@testing-library/react';
import { MockedProvider } from '@apollo/client/testing';

import  App from './App';
import STATIONS_QUERY from '../queries/station/station';

const validMocks = [
  {
    request: {
      query: STATIONS_QUERY,
    },
    result: {
      data: {
        station: [
          {
            station_ID: 1014,
            custom_evse_id: null,
            location_ID: 1014,
            seller_ID: 8,
            name: 'Abcd',
            connected: 0,
            position: '60.169584741559,24.93819450566',
            available: 0,
            lastconnect: '2015-02-13T08:03:27.000Z',
            roaming_identifier_cpo: 'FI*001',
          },
        ],
      },
    },
  },
];

const errorMocks = [
  {
    request: {
      query: STATIONS_QUERY,
    },
    result: {
      data: {
        station: [
          {
            station_ID: 1014,
            custom_evse_id: null,
            location_ID: 1014,
            seller_ID: 8,
            name: 'Abcd',
            connected: 0,
            position: '60.169584741559,24.93819450566',
            available: 0,
            lastconnect: '2015-02-13T08:03:27.000Z',
            roaming_identifier_cpo: 'FI*001',
          },
        ],
      },
    },
    error: new Error('Error!'),
  },
];

test('render loading state', () => {
  const { container } = render(
    <MockedProvider mocks={validMocks}>
      < App />
    </MockedProvider>
  );
  expect(container).toMatchSnapshot();
});

test('render loaded state', async () => {
  const { container, getByRole } = render(
    <MockedProvider mocks={validMocks}>
      < App />
    </MockedProvider>
  );
  await waitFor(() => {
    getByRole('menuitem');
    expect(container).toMatchSnapshot();
  });
});
