import React, { useState, useEffect } from 'react';
import StationsList from '../views/StationsList';
import Station from '../views/Station';
import Error from '../views/Error';
import Loading from '../views/Loading';

import useStation from '../queries/station/useStation';

const Main: React.FC = () => {
  const [
    expandedStationID,
    setExpandedStationID,
  ] = useState<number | undefined>(undefined);

  const [
    focusedItemId,
    setFocusedItemId,
  ] = useState<number | undefined>(undefined);

  const [
    stationsLoading,
    stationsError,
    data,
  ] = useStation(expandedStationID);

  useEffect(() => {
    setFocusedItemId(expandedStationID);
  }, [expandedStationID]);

  const goBack = () => { setExpandedStationID(undefined); };

  if (stationsLoading) {
    return <Loading />;
  }

  if (stationsError && data === undefined) {
    return (
      <Error
        isSingleStation={expandedStationID !== undefined}
        apolloErrors={stationsError}
        onClick={goBack}
      />
    );
  }

  const { station } = data;
  if (expandedStationID !== undefined) {
    return (
      <Station
        onClose={goBack}
        station={station[0]}
      />
    );
  }

  return (
    <StationsList
      focusedItemId={focusedItemId}
      onExpand={({ station_ID }) => {
        setExpandedStationID(station_ID);
      }}
      stations={station}
    />
  );
};

export default Main;
