import React, { useEffect, useRef } from 'react';
import Status from '../../components/Status';
import ListItem from '../../components/ListItem';
import AppLayout from '../../components/AppLayout';
import List from '../../components/List';
import AppBanner from '../../components/AppBanner';
import AppContent from '../../components/AppContent';
import Typography from '../../components/Typography';
import KEYS from '../../utils/keys';
import { StationListItem as StationListItemType } from '../../types';

const LIST_ITEM_CONFIG = {
  role: 'menuitem',
  tabIndex: 0,
};

const LIST_CONFIG = {
  role: 'menu',
};

const LIST_TITLE = 'Your stations';

const isValidKeyDown = (
  event: React.KeyboardEvent<HTMLLIElement>,
  fsn: () => void,
) => (event.key === KEYS.SPACE || event.key === KEYS.ENTER) && fsn();

const getStationStatus = (available: number) => available === 0 ? 'offline' : 'available';

interface Props {
  focusedItemId: number | undefined;
  onExpand: (station: StationListItemType) => void;
  stations: StationListItemType[];
}

const StationsList: React.FC<Props> = ({
  focusedItemId,
  onExpand,
  stations,
}) => {
  const itemsRefs = useRef<HTMLLIElement[] | null[]>([]);
  useEffect(() => {
    const focusingIndex = stations.findIndex(
      (station) => station.station_ID === focusedItemId,
    );
    itemsRefs.current[focusingIndex]?.focus();
  });
  return (
    <AppLayout>
      <AppBanner>
        <Typography>{LIST_TITLE}</Typography>
      </AppBanner>
      <AppContent>
        <List {...LIST_CONFIG}>
          {stations.map((station, index) => (
            <ListItem
              ref={(el) => {
                itemsRefs.current[index] = el;
              }}
              key={station.station_ID}
              onClick={() => onExpand(station)}
              onKeyDown={(event) => {
                isValidKeyDown(event, () => {
                  onExpand(station);
                });
              }}
              {...LIST_ITEM_CONFIG}
            >
              <Typography typography="h2" as="span">
                {station.name}
              </Typography>
              <Status status={getStationStatus(station.available)} />
            </ListItem>
          ))}
        </List>
      </AppContent>
    </AppLayout>
  );
};

export default StationsList;
