import React from 'react';
import { render, fireEvent } from '@testing-library/react';

import StationList from '.';

const STATIONS = [
  {
    station_ID: 1014,
    custom_evse_id: null,
    location_ID: 1014,
    seller_ID: 8,
    name: 'Abcd',
    connected: 0,
    position: '60.169584741559,24.93819450566',
    available: 0,
    lastconnect: '2015-02-13T08:03:27.000Z',
    roaming_identifier_cpo: 'FI*001',
  },
  {
    station_ID: 1011,
    custom_evse_id: null,
    location_ID: 1014,
    seller_ID: 8,
    name: 'Abcd - 2',
    connected: 0,
    position: '60.169584741559,24.93819450566',
    available: 1,
    lastconnect: '2015-02-13T08:03:27.000Z',
    roaming_identifier_cpo: 'FI*001',
  },
];

test('render focused state', () => {
  const { container } = render(
    <StationList onExpand={() => {}} focusedItemId={0} stations={STATIONS} />
  );
  expect(container).toMatchSnapshot();
});

test('call onExpand', async () => {
  const handleOnExpand = jest.fn();
  const { getByRole } = render(
    <StationList
      onExpand={handleOnExpand}
      focusedItemId={0}
      stations={[STATIONS[0]]}
    />
  );
  const menuItem = getByRole('menuitem');
  fireEvent.keyDown(menuItem, { key: 'Enter', code: 'Enter' });
  expect(handleOnExpand).toHaveBeenCalledWith(STATIONS[0]);
});
