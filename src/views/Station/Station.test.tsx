import React from 'react';
import { render, fireEvent } from '@testing-library/react';

import Station from '.';

const MOCK_STATION = {
  station_ID: 1014,
  custom_evse_id: null,
  location_ID: 1014,
  seller_ID: 8,
  name: 'Abcd',
  connected: 0,
  position: '60.169584741559,24.93819450566',
  available: 0,
  lastconnect: '2015-02-13T08:03:27.000Z',
  roaming_identifier_cpo: 'FI*001',
};

test('render default status', () => {
  const { container } = render(
    <Station station={MOCK_STATION} onClose={() => {}} />
  );
  expect(container).toMatchSnapshot();
});

test('call onClose', async () => {
  const handleOnClose = jest.fn();
  const { getByRole } = render(
    <Station station={MOCK_STATION} onClose={handleOnClose} />
  );
  const button = getByRole('button');
  fireEvent.keyDown(button, { key: 'Enter', code: 'Enter' });
  expect(handleOnClose).toHaveBeenCalledWith();
});

test('focuses on the button', () => {
  const { getByRole } = render(
    <Station station={MOCK_STATION} onClose={() => {}} />
  );
  const button = getByRole('button');
  expect(document.activeElement).toEqual(button);
});
