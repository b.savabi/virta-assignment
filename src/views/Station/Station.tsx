import React, { useEffect, useRef } from 'react';
import AppBanner from '../../components/AppBanner';
import AppLayout from '../../components/AppLayout';
import AppContent from '../../components/AppContent';
import DescriptionList from '../../components/DescriptionList';
import DescriptionItem from '../../components/DescriptionItem';
import Typography from '../../components/Typography';
import Button from '../../components/Button';
import Icon from '../../components/Icon';
import ArrowIcon from '../../icons/arrow.svg';
import KEYS from '../../utils/keys';
import Spacing from '../../tokens/spacing';
import { Station as StationType } from '../../types';

const isValidKeyDown = (
  event: React.KeyboardEvent<HTMLButtonElement>,
  fsn: () => void,
) => (event.key === KEYS.SPACE || event.key === KEYS.ENTER) && fsn();

interface Props {
  onClose: () => void;
  station: StationType;
}

const getValue = (station: Record<string, unknown>, key: string) => `${station[key]}`;

const BUTTON_CONFIG = {
  style: { marginRight: Spacing[24] },
};

const ICON_CONFIG = {
  style: { width: 26, height: 20 },
};

const Station: React.FC<Props> = ({ onClose, station }) => {
  const buttonRef = useRef<HTMLButtonElement>(null);
  useEffect(() => {
    if (buttonRef.current) {
      buttonRef.current.focus();
    }
  });

  return (
    <AppLayout>
      <AppBanner>
        <Button
          ref={buttonRef}
          onClick={onClose}
          onKeyDown={(event) => {
            isValidKeyDown(event, onClose);
          }}
          {...BUTTON_CONFIG}
        >
          <Icon {...ICON_CONFIG} src={ArrowIcon} />
        </Button>
        {station.name !== null && <Typography>{station.name}</Typography>}
      </AppBanner>
      <AppContent>
        <DescriptionList>
          {Object.keys(station).map((key) => (
            <DescriptionItem
              key={key}
              title={key}
              description={getValue(station, key)}
            />
          ))}
        </DescriptionList>
      </AppContent>
    </AppLayout>
  );
};

export default Station;
