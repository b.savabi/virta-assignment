import React from 'react';
import {
  ApolloError,
} from '@apollo/client';
import Alert from '../../components/Alert';
import Button from '../../components/Button';
import AppLayout from '../../components/AppLayout';
import Spacing from '../../tokens/spacing';

interface Props {
  apolloErrors: ApolloError,
  isSingleStation: boolean,
  onClick: React.MouseEventHandler<HTMLButtonElement>;
}

const BUTTON_CONFIG = {
  autoFocus: true,
  style: {
    marginTop: Spacing[12],
  },
};

const Error: React.FC<Props> = ({ apolloErrors, isSingleStation, onClick }) => (
  <AppLayout>
    {apolloErrors.graphQLErrors && apolloErrors.graphQLErrors.map(
      (graphQLError: { message: string }) => (
        <Alert key={graphQLError.message}>
          {graphQLError.message}
        </Alert>
      ),
    )}
    {apolloErrors.networkError && apolloErrors.networkError
      && (
        <Alert>
          {apolloErrors.networkError.message}
        </Alert>
      )}
    {isSingleStation && <Button {...BUTTON_CONFIG} onClick={onClick}>Go back</Button>}
  </AppLayout>
);

export default Error;
