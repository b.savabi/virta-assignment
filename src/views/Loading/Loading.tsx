import React from 'react';
import Alert from '../../components/Alert';
import AppLayout from '../../components/AppLayout';

const MSG_LOADING = 'Fetching data from a GraphQL server on Heroku, the app might be idling when it takes a long time to reload.';

const Loading: React.FC = () => (
  <AppLayout>
    <Alert aria-busy>{MSG_LOADING}</Alert>
  </AppLayout>
);

export default Loading;
