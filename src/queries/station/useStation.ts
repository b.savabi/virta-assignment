import {
  useQuery,
  ApolloError,
} from '@apollo/client';

import { Station as StationType } from '../../types';

import STATION_QUERY from './station';

const useStation = (stationID?: number): [
  boolean,
  ApolloError | undefined,
  { station: [StationType] },
] => {
  const {
    loading,
    error,
    data,
  } = useQuery(STATION_QUERY, {
    variables: {
      station_ID: stationID,
    },
    notifyOnNetworkStatusChange: true,
  });
  return [loading, error, data];
};

export default useStation;
