import { gql } from '@apollo/client';

export default gql`
  query station($station_ID: Int) {
    station(station_ID: $station_ID) {
      station_ID
      custom_evse_id
      location_ID
      seller_ID
      name
      connected
      position
      available
      lastconnect
      roaming_identifier_cpo
    }
  }
`;
