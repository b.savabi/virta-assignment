export type Station = {
  station_ID: number;
  custom_evse_id: string | null;
  location_ID: number;
  seller_ID: number;
  name: string;
  connected: number;
  position: string;
  available: number;
  lastconnect: string;
  roaming_identifier_cpo: string | null;
};

export type StationListItem = {
  station_ID: number;
  custom_evse_id: string | null;
  location_ID: number;
  seller_ID: number;
  name: string;
  connected: number;
  position: string;
  available: number;
  lastconnect: string;
  roaming_identifier_cpo: string | null;
};
