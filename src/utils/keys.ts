const KEY_CODES = {
  SPACE: ' ',
  ENTER: 'Enter',
};

export default KEY_CODES;
