const Dimension = {
  14: '14px',
  24: '24px',
  64: '64px',
  48: '48px',
  102: '102px',
  608: '608px',
  contentWidth: {
    width: '608px',
  },
};

export default Dimension;
