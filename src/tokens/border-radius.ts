const BorderRadius = {
  8: '8px',
  24: '24px',
};

export default BorderRadius;
