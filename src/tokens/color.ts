const Color = {
  brightContent: '#efefef',
  lightContainer: '#ffffff',
  darkContainer: '#f2f2f2',
  content: '#222222',
  interactiveContainer: '#222222',
  focusedBorder: '#ff1faf',
  descriptionTitle: '#5f5f5f',
};

export default Color;
