import styled from 'styled-components';
import Dimension from '../../tokens/dimension';

const styledElements = {
  Icon: styled.img`
    height: ${Dimension[14]};
    width: ${Dimension[14]};
  `,
};

export default styledElements;
