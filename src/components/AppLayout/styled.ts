import styled from 'styled-components';
import Color from '../../tokens/color';
import Spacing from '../../tokens/spacing';

const styledElements = {
  AppLayout: styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: flex-start;
    overflow: hidden;
    min-height: 100vh;
    width: 100vw;
    background: ${Color.darkContainer};
    padding-top: ${Spacing[48]};
    padding-bottom: ${Spacing[48]};
  `,
};

export default styledElements;
