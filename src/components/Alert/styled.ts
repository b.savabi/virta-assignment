import styled, { css } from 'styled-components';
import Color from '../../tokens/color';
import Typography from '../../tokens/typography';
import BorderRadius from '../../tokens/border-radius';
import Spacing from '../../tokens/spacing';
import Dimension from '../../tokens/dimension';

const styledElements = {
  Alert: styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    & > span {
      color: ${Color.content};
      background: ${Color.lightContainer};
      padding: ${Spacing[12]};
      border-radius: ${BorderRadius[8]};
      text-align: center;
      ${css`
        ${Dimension.contentWidth}
        ${Typography.h2}
      `}
    }
  `,
};

export default styledElements;
