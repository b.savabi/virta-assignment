import React from 'react';
import Styled from './styled';

interface Props {
  /**
   * The content of the `Alert`.
   */
  children: string;
}

const ALERT = {
  role: 'alert',
};

/**
 * `Alert` component, can be used to render alerts.
 * All props will be passed to the root element.
 */
const Alert: React.FC<Props> = ({ children, ...otherProps }) => (
  <Styled.Alert {...ALERT} {...otherProps}>
    <span>{children}</span>
  </Styled.Alert>
);

export default Alert;
