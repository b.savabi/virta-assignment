import React from 'react';
import { render } from '@testing-library/react';

import Alert from '.';

test('render default status', () => {
  const { container } = render(<Alert>Message</Alert>);
  expect(container).toMatchSnapshot();
});

test('should pass custom attributes', () => {
  const { container } = render(<Alert data-test="id-1">Message</Alert>);
  expect(container).toMatchSnapshot();
});
