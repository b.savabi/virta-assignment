import styled from 'styled-components';
import Color from '../../tokens/color';
import BorderRadius from '../../tokens/border-radius';
import Spacing from '../../tokens/spacing';
import Dimension from '../../tokens/dimension';
import BorderWidth from '../../tokens/border-width';

const styledElements = {
  ListItem: styled.li`
    cursor: pointer;
    display: flex;
    align-items: center;
    justify-content: space-between;
    overflow: hidden;
    background: ${Color.lightContainer};
    border-radius: ${BorderRadius[8]};
    padding: 0 ${Spacing[16]};
    height: ${Dimension[64]};
    border: solid transparent ${BorderWidth[2]};
    &:focus {
      outline: none;
      border-color: ${Color.focusedBorder};
    }
  `,
};

export default styledElements;
