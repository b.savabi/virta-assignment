import React from 'react';
import { render } from '@testing-library/react';

import ListItem from '.';

test('render content', () => {
  const { container } = render(
    <ListItem>
      <div className="content">content</div>
    </ListItem>
  );
  expect(container).toMatchSnapshot();
});

test('should pass custom attributes', () => {
  const { container } = render(
    <ListItem data-test="id-01">
      <div className="content">content</div>
    </ListItem>
  );
  expect(container).toMatchSnapshot();
});
