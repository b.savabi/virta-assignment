import React from 'react';
import Styled from './styled';

interface Props {
  /**
   * Callback fired when user presses any key on the list item.
   *
   * @param {object} event The event source of the callback.
   */
  onKeyDown?: React.KeyboardEventHandler<HTMLLIElement>;
  /**
   * Callback fired when user clicks on the list item.
   *
   * @param {object} event The event source of the callback.
   */
  onClick?: React.MouseEventHandler<HTMLLIElement>;
  /** tabIndex of the list item element. */
  tabIndex?: number;
  /** The role of the list item element. */
  role?: string;
  /** Content of the list item. */
  children: React.ReactNode;
}

/**
 * `ListItem` can be used to render a `List`'s items. It mimics the `li` element https://developer.mozilla.org/en-US/docs/Web/HTML/Element/li.
 * All props will be passed to the root element.
 */
const ListItem = React.forwardRef<HTMLLIElement, Props>((props, ref) => {
  const { children, ...otherProps } = props;
  return (
    <Styled.ListItem ref={ref} {...otherProps}>
      {children}
    </Styled.ListItem>
  );
});

export default ListItem;
