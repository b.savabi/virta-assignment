import React from 'react';
import { render } from '@testing-library/react';

import Typography from '.';

test('render default variant', () => {
  const { container } = render(<Typography>Content</Typography>);
  expect(container).toMatchSnapshot();
});

test('render h2 variant', () => {
  const { container } = render(
    <Typography typography="h2">Content</Typography>
  );
  expect(container).toMatchSnapshot();
});

test('should pass custom attributes', () => {
  const { container } = render(
    <Typography data-test="id-1">Contnet</Typography>
  );
  expect(container).toMatchSnapshot();
});
