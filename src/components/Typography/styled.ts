import styled from 'styled-components';
import Color from '../../tokens/color';

const styledElements = {
  Typography: styled.h1`
    max-width: 100%;
    overflow: hidden;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    color: ${Color.content};
  `,
};

export default styledElements;
