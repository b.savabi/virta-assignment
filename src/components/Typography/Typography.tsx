import React from 'react';
import Styled from './styled';
import TypographyToken from '../../tokens/typography';

const DEFAULT_TYPOGRAPHY = 'h1';

const TYPOGRAPHIES = {
  h1: {
    as: 'h1',
    fontStyle: TypographyToken.h1,
  },
  h2: {
    as: 'h2',
    fontStyle: TypographyToken.h2,
  },
};

interface Props {
  /** The text of the typography. */
  children: string;
  /** The element tag name. */
  as?: React.ElementType;
  /** The typography variant. */
  typography?: 'h1' | 'h2';
  /** To override the style. */
  style?: Record<string, unknown>;
}

/**
 * `Typography` can be used to render text on the UI with predefined styles.
 * All props will be passed to the root element.
 */
const Typography: React.FC<Props> = ({
  children,
  as: customAs,
  style,
  typography = DEFAULT_TYPOGRAPHY,
  ...otherProps
}) => {
  const { as, fontStyle } = TYPOGRAPHIES[typography];

  const resolvedAs = customAs !== undefined ? customAs : as;

  const resolvedStyle = {
    ...fontStyle,
    ...style,
  };

  return (
    <Styled.Typography as={resolvedAs} style={resolvedStyle} {...otherProps}>
      {children}
    </Styled.Typography>
  );
};

export default Typography;
