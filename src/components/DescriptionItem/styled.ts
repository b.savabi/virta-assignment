import styled, { css } from 'styled-components';
import Color from '../../tokens/color';
import Spacing from '../../tokens/spacing';
import Typography from '../../tokens/typography';

const styledElements = {
  DescriptionItem: styled.div`
    display: flex;
    flex-direction: column;
    overflow: hidden;
    width: calc(25% - ${Spacing[24]});
    margin: ${Spacing[10]} ${Spacing[12]};
  `,
  DescriptionTitle: styled.dt`
    ${css`
      ${Typography.description}
    `};
    color: ${Color.descriptionTitle};
    overflow: hidden;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  `,
  DescriptionDescription: styled.dd`
    ${css`
      ${Typography.description}
    `};
    color: ${Color.content};
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  `,
};

export default styledElements;
