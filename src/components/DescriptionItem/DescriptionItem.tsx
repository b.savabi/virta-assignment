import React from 'react';
import Styled from './styled';

interface Props {
  /** Description item title. */
  title: string;
  /** Description item description. */
  description: string | null | number;
}

/**
 * `DescriptionItem` can be used to render the title
 * and description of a description item in a `DescriptionList`.
 * All props will be passed to the root element.
 */
const DescriptionItem: React.FC<Props> = ({
  title,
  description,
  ...otherProps
}) => (
  <Styled.DescriptionItem>
    <Styled.DescriptionTitle
      {...otherProps}
    >
      {title}
    </Styled.DescriptionTitle>
    <Styled.DescriptionDescription {...otherProps}>
      {description}
    </Styled.DescriptionDescription>
  </Styled.DescriptionItem>
);

export default DescriptionItem;
