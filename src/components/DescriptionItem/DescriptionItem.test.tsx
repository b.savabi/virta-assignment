import React from 'react';
import { render } from '@testing-library/react';

import DescriptionItem from '.';

test('render content', () => {
  const { container } = render(
    <DescriptionItem title="title" description="description" />
  );
  expect(container).toMatchSnapshot();
});

test('should pass custom attributes', () => {
  const { container } = render(
    <DescriptionItem
      data-test="id-01"
      title="title"
      description="description"
    />
  );
  expect(container).toMatchSnapshot();
});
