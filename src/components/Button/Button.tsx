import React from 'react';
import Styled from './styled';

interface Props {
  /**
   * Callback fired when user presses any key on the button.
   *
   * @param {object} event The event source of the callback.
   */
  onKeyDown?: React.KeyboardEventHandler<HTMLButtonElement>;
  /**
   * Callback fired when user clicks on the button.
   *
   * @param {object} event The event source of the callback.
   */
  onClick?: React.MouseEventHandler<HTMLButtonElement>;
  /** Styles applied to the root element. */
  style?: Record<string, unknown>;
  /** The content of the Button. */
  children: React.ReactNode;
}

/**
 * `Button` can be used to render button.
 * All props will be passed to the root element.
 */
export const Button = React.forwardRef<HTMLButtonElement, Props>(
  (props, ref) => {
    const { style = {}, children, ...otherProps } = props;
    return (
      <Styled.Button ref={ref} style={style} {...otherProps}>
        {children}
      </Styled.Button>
    );
  },
);

export default Button;
