import styled, { css } from 'styled-components';
import Dimension from '../../tokens/dimension';
import Spacing from '../../tokens/spacing';

const styledElements = {
  AppContent: styled.div`
    margin-bottom: ${Spacing[48]};
    ${css`
      ${Dimension.contentWidth}
    `};
  `,
};

export default styledElements;
