import React from 'react';
import { render } from '@testing-library/react';

import AppContent from '.';

test('render content', () => {
  const { container } = render(
    <AppContent>
      <div className="content">content</div>
    </AppContent>
  );
  expect(container).toMatchSnapshot();
});

test('should pass custom attributes', () => {
  const { container } = render(
    <AppContent data-test="id-01">
      <div className="content">content</div>
    </AppContent>
  );
  expect(container).toMatchSnapshot();
});
