import React from 'react';
import Styled from './styled';

interface Props {
  /**
   * The content of the app.
   */
  children: React.ReactNode;
}

/**
 * `AppContent` can be used to wrap the content the app.
 * All props will be passed to the root element.
 */
const AppContent: React.FC<Props> = ({ children, ...otherProps }) => (
  <Styled.AppContent {...otherProps}>{children}</Styled.AppContent>
);

export default AppContent;
