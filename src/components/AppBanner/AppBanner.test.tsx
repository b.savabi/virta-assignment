import React from 'react';
import { render } from '@testing-library/react';

import AppBanner from '.';

test('render content', () => {
  const { container } = render(
    <AppBanner>
      <div className="content">content</div>
    </AppBanner>
  );
  expect(container).toMatchSnapshot();
});

test('should pass custom attributes', () => {
  const { container } = render(
    <AppBanner data-test="id-01">
      <div className="content">content</div>
    </AppBanner>
  );
  expect(container).toMatchSnapshot();
});
