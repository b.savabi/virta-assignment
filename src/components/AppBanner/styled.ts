import styled, { css } from 'styled-components';
import Color from '../../tokens/color';
import Dimension from '../../tokens/dimension';
import Spacing from '../../tokens/spacing';

const styledElements = {
  AppBanner: styled.div`
    display: flex;
    align-items: center;
    background: ${Color.darkContainer};
    margin-bottom: ${Spacing[48]} 0;
    ${css`
      ${Dimension.contentWidth}
    `}
  `,
};

export default styledElements;
