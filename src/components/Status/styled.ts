import styled, { css } from 'styled-components';
import Color from '../../tokens/color';
import Typography from '../../tokens/typography';
import BorderRadius from '../../tokens/border-radius';
import Spacing from '../../tokens/spacing';
import Dimension from '../../tokens/dimension';

const styledElements = {
  Status: styled.div`
    display: inline-flex;
    align-items: center;
    overflow: hidden;
    min-width: ${Dimension[102]};
    height: ${Dimension[24]};
    background: ${Color.darkContainer};
    border-radius: ${BorderRadius[24]};
    padding: 0 ${Spacing[6]};
    ${css`
      ${Typography.info}
    `};
    color: ${Color.content};
  `,
};

export default styledElements;
