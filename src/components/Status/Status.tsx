import React from 'react';
import Styled from './styled';
import Icon from '../Icon';
import Spacing from '../../tokens/spacing';

import AvailableIcon from '../../icons/available.svg';
import OfflineIcon from '../../icons/offline.svg';

const DEFAULT_STATUS = 'available';

const STATUS_CONFIG = {
  role: 'status',
};

const STATUSES = {
  available: {
    label: 'Available',
    icon: AvailableIcon,
  },
  offline: {
    label: 'Offline',
    icon: OfflineIcon,
  },
};

const ICON_CONFIG = {
  style: {
    marginRight: Spacing[6],
  },
};

interface Props {
  /** The status. */
  status?: 'available' | 'offline';
  /** Label of status. */
  label?: string;
  /** To override the icon of the status. Consider using `Icon` component. */
  icon?: React.ReactNode;
}

/**
 * `Status` can be used to render "available" or "offline" statuses.
 * All props will be passed to the root element.
 */
const Status: React.FC<Props> = ({
  icon: customIcon,
  label: customLabel,
  status = DEFAULT_STATUS,
  ...otherProps
}) => {
  const { icon, label } = STATUSES[status];

  const resolvedLabel = customLabel !== undefined ? customLabel : label;

  const resolvedIcon = customIcon !== undefined
    ? (
      customIcon
    ) : (
      <Icon {...ICON_CONFIG} src={icon} alt={label} />
    );

  return (
    <Styled.Status {...STATUS_CONFIG} {...otherProps}>
      {resolvedIcon}
      {resolvedLabel}
    </Styled.Status>
  );
};

export default Status;
