import React from 'react';
import { render } from '@testing-library/react';

import Status from '.';

test('render default status', () => {
  const { container } = render(<Status />);
  expect(container).toMatchSnapshot();
});

test('render Available status', () => {
  const { container } = render(<Status status="available" />);
  expect(container).toMatchSnapshot();
});

test('render Offline status', () => {
  const { container } = render(<Status status="offline" />);
  expect(container).toMatchSnapshot();
});

test('render custom status', () => {
  const CUSTOM_LABEL = 'Custom label';
  const { container } = render(<Status label={CUSTOM_LABEL} />);
  expect(container).toMatchSnapshot();
});

test('render Available icon', () => {
  const { container } = render(<Status />);
  expect(container).toMatchSnapshot();
});

test('render Offline icon', () => {
  const { container } = render(<Status status="offline" />);
  expect(container).toMatchSnapshot();
});

test('render custom icon', () => {
  const { container } = render(<Status icon={<img alt="empty" />} />);
  expect(container).toMatchSnapshot();
});

test('should pass custom attributes', () => {
  const { container } = render(<Status data-test="id-1" />);
  expect(container).toMatchSnapshot();
});
