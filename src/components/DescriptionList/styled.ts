import styled from 'styled-components';
import Spacing from '../../tokens/spacing';
import Color from '../../tokens/color';
import BorderRadius from '../../tokens/border-radius';

const styledElements = {
  DescriptionList: styled.dl`
    display: flex;
    overflow: hidden;
    flex-wrap: wrap;
    padding: ${Spacing[10]} ${Spacing[12]};
    background: ${Color.lightContainer};
    border-radius: ${BorderRadius[8]};
    width: 100%;
  `,
};

export default styledElements;
