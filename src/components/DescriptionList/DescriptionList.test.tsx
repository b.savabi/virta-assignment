import React from 'react';
import { render } from '@testing-library/react';

import DescriptionList from '.';

test('render content', () => {
  const { container } = render(
    <DescriptionList>
      <div>
        <dt className="content">content</dt>
        <dd className="content">content</dd>
      </div>
    </DescriptionList>
  );
  expect(container).toMatchSnapshot();
});

test('should pass custom attributes', () => {
  const { container } = render(
    <DescriptionList data-test="id-01">
      <div>
        <dt className="content">content</dt>
        <dd className="content">content</dd>
      </div>
    </DescriptionList>
  );
  expect(container).toMatchSnapshot();
});
