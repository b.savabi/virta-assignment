import React from 'react';
import Styled from './styled';

interface Props {
  /** The role of the dl element. */
  role?: string;
  /** The content of the dl, consider using `DescriptionItem`. */
  children: React.ReactNode;
}

/**
 * `DescriptionList` can be used to render a description list.
 * It mimics the `dl` element https://developer.mozilla.org/en-US/docs/Web/HTML/Element/dl.
 * All props will be passed to the root element.
 */
const DescriptionList: React.FC<Props> = ({ children, ...otherProps }) => (
  <Styled.DescriptionList {...otherProps}>{children}</Styled.DescriptionList>
);

export default DescriptionList;
