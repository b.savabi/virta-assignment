import React from 'react';
import Styled from './styled';

interface Props {
  /** The role of the `ul` element. */
  role?: string;
  /** The content of `List`, consider using ListItem`. */
  children: React.ReactNode;
}

/**
 * `List` can be used to render a list. It mimics the `ul` element https://developer.mozilla.org/en-US/docs/Web/HTML/Element/ul.
 * All props will be passed to the root element.
 */
const List: React.FC<Props> = ({ children, ...otherProps }) => (
  <Styled.List {...otherProps}>{children}</Styled.List>
);

export default List;
