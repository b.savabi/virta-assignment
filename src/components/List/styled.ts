import styled from 'styled-components';
import Spacing from '../../tokens/spacing';

const styledElements = {
  List: styled.ul`
    overflow: hidden;
    width: 100%;
    & > *:not(:last-child) {
      margin-bottom: ${Spacing[8]};
    }
  `,
};

export default styledElements;
