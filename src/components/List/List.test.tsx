import React from 'react';
import { render } from '@testing-library/react';

import List from '.';

test('render content', () => {
  const { container } = render(
    <List>
      <li className="content">content</li>
    </List>
  );
  expect(container).toMatchSnapshot();
});

test('should pass custom attributes', () => {
  const { container } = render(
    <List data-test="id-01">
      <li className="content">content</li>
    </List>
  );
  expect(container).toMatchSnapshot();
});
